FROM ubuntu:latest
# Builds Procheck.
ENV PROCHECK_VERSION 3.5.4
ADD procheck.tar.gz /opt/
RUN apt-get update -y 
RUN apt-get install -y csh gfortran make 
ENV PRODIR /opt/procheck
RUN cd ${PRODIR} && make
RUN echo 'set prodir = /opt/procheck' > /root/.cshrc && \
    echo 'setenv prodir /opt/procheck' >> /root/.cshrc && \
    echo 'alias procheck ${prodir}/procheck.scr' >> /root/.cshrc && \
    echo 'alias procheck_comp ${prodir}/procheck_comp.scr' >> /root/.cshrc && \
    echo 'alias procheck_nmr ${prodir}/procheck_nmr.scr' >> /root/.cshrc && \
    echo 'alias proplot ${prodir}/proplot.scr' >> /root/.cshrc && \
    echo 'alias proplot_comp ${prodir}/proplot_comp.scr' >> /root/.cshrc && \
    echo 'alias proplot_nmr ${prodir}/proplot_nmr.scr' >> /root/.cshrc && \
    echo 'alias gfac2pdb ${prodir}/gfac2pdb.scr' >> /root/.cshrc && \
    echo 'alias viol2pdb ${prodir}/viol2pdb.scr' >> /root/.cshrc && \
    echo 'alias wirplot ${prodir}/wirplot.scr' >> /root/.cshrc 
ENTRYPOINT ["/opt/procheck/procheck.scr"]
CMD []
