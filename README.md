A Dockerized version of Procheck v 3.5.4
========================================

This repo provides Procheck in a dockerized way. Instead of 
installing the software, you install "shims" with the same names as the key 
Procheck scripts that, when run, download (if neccessary) and then launch 
a Docker container to actually execute the command.


*NOTE*: If you are a new Procheck user, please complete and return the confidentiality agreement [here](https://www.ebi.ac.uk/thornton-srv/software/PROCHECK/) before use.

Installation:
-------------
The easy way:
```
% pip install git+https://bitbucket.org/claughton/procheck_docker.git@v3.5.4
```

or:
```
% git clone https://bitbucket.org/claughton/procheck_docker.git
% cd procheck_docker
% git checkout v3.5.4
% make install
```

To remove:
```
% make uninstall
```

Usage:
------

The installation adds two  scripts to /usr/local/bin (or wherever):

* procheck: provides a shim for the Procheck 'procheck' command.
* gfac2pdb: provides a shim for the Procheck 'gfac2pdb' utility.

In the example folder is an example PDB file you can use to test your installation.

Rebuilding the Docker image:
----------------------------

If you want to build your own copy of the image you will need to
obtain your own copy of the compressed tarball for Procheck by following the links from [here](https://www.ebi.ac.uk/thornton-srv/software/PROCHECK/). 
Place this file in the procheck_docker directory. Next edit the Makefile to set your own Docker username, then:
```
% make build
% make publish
```
