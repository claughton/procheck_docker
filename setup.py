from setuptools import setup, find_packages
setup(
    name = 'procheckdocker',
    version = '3.5.4',
    packages = find_packages(),
    scripts = [
        'scripts/procheck',
        'scripts/gfac2pdb',
    ],
)
