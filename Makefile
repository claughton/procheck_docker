PREFIX ?= /usr/local
VERSION = 3.5.4
DOCKERID = claughton

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/procheck $(DESTDIR)$(PREFIX)/bin/procheck
	install -m 0755 scripts/gfac2pdb $(DESTDIR)$(PREFIX)/bin/gfac2pdb

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/procheck
	@$(RM) $(DESTDIR)$(PREFIX)/bin/gfac2pdb
	@docker rmi $(DOCKERID)/procheck:$(VERSION)
	@docker rmi $(DOCKERID)/procheck:latest

build: Dockerfile
	@docker build -t $(DOCKERID)/procheck:$(VERSION) . \
	&& docker tag $(DOCKERID)/procheck:$(VERSION) $(DOCKERID)/procheck:latest

publish: build
	@docker push $(DOCKERID)/procheck:$(VERSION) \
	&& docker push $(DOCKERID)/procheck:latest

.PHONY: all install uninstall build publish
